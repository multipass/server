class AddReferenceToBuyingHistory < ActiveRecord::Migration[5.2]
  def change
    add_reference :buying_histories, :client, foreign_key: true
    add_reference :buying_histories, :stores, foreign_key: true
  end
end
