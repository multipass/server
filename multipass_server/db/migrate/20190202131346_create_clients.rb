class CreateClients < ActiveRecord::Migration[5.2]
  def change
    create_table :clients do |t|
      t.string :firstname
      t.string :name
      t.date :birthday
      t.string :card_id

      t.timestamps
    end
  end
end
