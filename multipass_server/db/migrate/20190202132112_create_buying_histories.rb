class CreateBuyingHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :buying_histories do |t|
      t.timestamps
    end
  end
end
