# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_02_160413) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "buying_histories", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "client_id"
    t.bigint "stores_id"
    t.index ["client_id"], name: "index_buying_histories_on_client_id"
    t.index ["stores_id"], name: "index_buying_histories_on_stores_id"
  end

  create_table "clients", force: :cascade do |t|
    t.string "firstname"
    t.string "name"
    t.date "birthday"
    t.string "card_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "store_id"
    t.bigint "buying_histories_id"
    t.index ["buying_histories_id"], name: "index_products_on_buying_histories_id"
    t.index ["store_id"], name: "index_products_on_store_id"
  end

  create_table "stores", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "buying_histories", "clients"
  add_foreign_key "buying_histories", "stores", column: "stores_id"
  add_foreign_key "products", "buying_histories", column: "buying_histories_id"
  add_foreign_key "products", "stores"
end
