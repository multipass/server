class BuyingHistoriesController < ApplicationController
  before_action :set_buying_history, only: [:show, :update, :destroy]

  # GET /buying_histories
  def index
    @buying_histories = BuyingHistory.all

    render json: @buying_histories
  end

  # GET /buying_histories/1
  def show
    render json: @buying_history
  end

  # POST /buying_histories
  def create
    @buying_history = BuyingHistory.new(buying_history_params)

    if @buying_history.save
      render json: @buying_history, status: :created, location: @buying_history
    else
      render json: @buying_history.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /buying_histories/1
  def update
    if @buying_history.update(buying_history_params)
      render json: @buying_history
    else
      render json: @buying_history.errors, status: :unprocessable_entity
    end
  end

  # DELETE /buying_histories/1
  def destroy
    @buying_history.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_buying_history
      @buying_history = BuyingHistory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def buying_history_params
      params.require(:buying_history).permit(:stores, :clients, :products)
    end
end
