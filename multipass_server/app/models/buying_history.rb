class BuyingHistory < ApplicationRecord
    belongs_to :client, class_name: "client", foreign_key: "client_id"
    belongs_to :store, class_name: "store", foreign_key: "store_id"
    has_many :product
end