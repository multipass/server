class Product < ApplicationRecord
    belongs_to :store, class_name: "store", foreign_key: "store_id"
    belongs_to :buying_history, class_name: "buying_history", foreign_key: "buying_history_id"
end
