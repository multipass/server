FactoryGirl.define do
  factory :client do
    firstname "MyString"
    name "MyString"
    birthday "2019-02-02"
    card_id "MyString"
  end
end
