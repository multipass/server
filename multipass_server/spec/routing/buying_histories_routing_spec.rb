require "rails_helper"

RSpec.describe BuyingHistoriesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/buying_histories").to route_to("buying_histories#index")
    end

    it "routes to #show" do
      expect(:get => "/buying_histories/1").to route_to("buying_histories#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/buying_histories").to route_to("buying_histories#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/buying_histories/1").to route_to("buying_histories#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/buying_histories/1").to route_to("buying_histories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/buying_histories/1").to route_to("buying_histories#destroy", :id => "1")
    end
  end
end
