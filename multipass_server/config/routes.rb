Rails.application.routes.draw do
  resources :buying_histories
  resources :stores
  resources :products
  resources :clients
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
