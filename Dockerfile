FROM ruby:2.5.3

RUN apt-get update && apt-get install -y git cmake && rm -rf /var/lib/apt/lists/*

RUN gem install bundler
